package com.report.generator.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.report.generator.entities.Hotel;

public class ChatbotResponse {

	public String answer;
	public List<Hotel> hotelInformation;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public List<Hotel> getHotelInformation() {
		return hotelInformation;
	}

	public void setHotelInformation(List<Hotel> hotelInformation) {
		this.hotelInformation = hotelInformation;
	}

	
	
}
