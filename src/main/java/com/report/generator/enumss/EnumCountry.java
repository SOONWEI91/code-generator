package com.report.generator.enumss;

public enum EnumCountry {
	Malaysia("MYS","Malaysia");
	
	private final String code;
    private final String value;

    private EnumCountry(String code, String value) {
        this.code = code;
        this.value = value;
    }

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
    
    
	
	
}
