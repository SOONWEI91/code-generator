package com.report.generator.enumss;

public enum EnumFunctions {
	Places("https://www.google.com/maps/place/"), 
	Direction("https://www.google.com/maps/dir/");
	
	private String value;

	EnumFunctions(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }
}
