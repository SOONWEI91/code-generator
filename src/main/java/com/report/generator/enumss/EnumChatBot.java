package com.report.generator.enumss;

public enum EnumChatBot {
	Travel("Where do you prefer for travel 1.Local 2.Foreign ?"), 
	Package("Where do you prefer for travel 1.Local 2.Foreign ?"), 
	Local("Which Location do you prefer to Local Trip ?"), 
	Foreign("Which Country do you prefer to Travel ?");
	
	private String value;

	EnumChatBot(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }
}
