package com.report.generator.controller;

import java.io.IOException;
import java.rmi.ServerException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.report.generator.entities.Customer;
import com.report.generator.model.ChatbotRequest;
import com.report.generator.model.ChatbotResponse;
import com.report.generator.service.CustomerEnquiryBot;
import com.report.generator.service.CustomerReport;

@Controller
public class CustomerController {

	@Autowired
	private CustomerReport service;
	
	@Autowired
	private CustomerEnquiryBot services;

	@GetMapping(value ="/users/export", produces = "application/csv")
	public void exportToCSV(HttpServletResponse response) throws IOException {
		response.setContentType("text/csv");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".csv";
		response.setHeader(headerKey, headerValue);

		List<Customer> listCustomers = service.listAll();

		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
		String[] csvHeader = { "User ID", "E-mail", "Full Name", "Status", "Enabled" };
		String[] nameMapping = { "customerId", "email", "fullName", "status", "enabled" };

		csvWriter.writeHeader(csvHeader);

		for (Customer customer : listCustomers) {
			csvWriter.write(customer, nameMapping);
		}
		

		csvWriter.close();

}
	
	@PostMapping(value ="/users/enquiry", produces = "application/json")
	public ResponseEntity<ChatbotResponse> userEnquiryBot(@RequestBody ChatbotRequest request) throws IOException {

		ChatbotResponse reply = services.ChatBot(request.getQuestion());

		        return new ResponseEntity<>(reply, HttpStatus.OK);	    
}
}