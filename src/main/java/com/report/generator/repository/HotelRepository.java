package com.report.generator.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.report.generator.entities.Hotel;


public interface HotelRepository extends JpaRepository<Hotel, Integer> {

	List<Hotel> findByCountryCode(String code);

	List<Hotel> findByLocation(String locationName);
     
}
 
 