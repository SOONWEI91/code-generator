package com.report.generator.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.report.generator.entities.Country;
import com.report.generator.entities.Location;


public interface LocationRepository extends JpaRepository<Location, Integer> {


}
 
 