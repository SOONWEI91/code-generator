package com.report.generator.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.report.generator.entities.Customer;


public interface CustomerRepository extends JpaRepository<Customer, Integer> {
     
}
 
 