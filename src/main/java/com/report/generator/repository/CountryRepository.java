package com.report.generator.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.report.generator.entities.Country;


public interface CountryRepository extends JpaRepository<Country, Integer> {

	Country findByCountryName(String ct);

}
 
 