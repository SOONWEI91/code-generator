package com.report.generator.entities;

import java.util.*;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
 
@Entity
@Table(name = "persons")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customerId")
    @JsonIgnore
    private Integer customerId;
     
    @Column(name = "email")
    @JsonIgnore
    private String email;
    
    @Column(name = "age")
    @JsonIgnore
    private String age;
     
    @Column(name = "fullName")
    @JsonIgnore
    private String fullName;
    
    @Column(name = "status")
    @JsonIgnore
    private String status;
    
    @Column(name = "enabled")
    @JsonIgnore
    private boolean enabled;
    
    @Column(name = "registerDate")
    @JsonIgnore
    private Date registerDate;

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
    
    
     
  /*  @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
            )
    private Set<Role> roles = new HashSet<>();*/
 
    // constructors, getter and setters are not shown for brevity
}