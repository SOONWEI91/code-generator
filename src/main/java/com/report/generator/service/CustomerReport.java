package com.report.generator.service;

import java.util.List;

import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.report.generator.entities.Customer;
import com.report.generator.repository.CustomerRepository;
 
@Service
@Transactional
public class CustomerReport {
     
    @Autowired
    private CustomerRepository repo;
     
    public List<Customer> listAll() {
        return repo.findAll(Sort.by("fullName").ascending());
    }
     
}
