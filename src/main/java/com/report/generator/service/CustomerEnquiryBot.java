package com.report.generator.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.report.generator.entities.*;
import com.report.generator.enumss.EnumChatBot;
import com.report.generator.enumss.EnumCountry;
import com.report.generator.enumss.EnumFunctions;
import com.report.generator.model.ChatbotResponse;
import com.report.generator.repository.*;

import org.apache.commons.lang3.StringUtils;

@Service
public class CustomerEnquiryBot {
	@Autowired
	private HotelRepository hotelrepo;
	@Autowired
	private CountryRepository countryrepo;
	@Autowired
	private LocationRepository locationrepo;

	public ChatbotResponse ChatBot(String question) {
		ChatbotResponse response = new ChatbotResponse();
		String answer = "";
		List<String> Chabot = Stream.of(EnumChatBot.values()).map(EnumChatBot::name).collect(Collectors.toList());

		// For FAQ criteria
		for (String c : Chabot) {
			if (StringUtils.containsIgnoreCase(question, c)) {
				switch (c) {
				case "Travel":
					answer = EnumChatBot.Travel.getValue();
					break;
				case "Package":
					answer = EnumChatBot.Package.getValue();
					break;
				case "Local":
					answer = EnumChatBot.Local.getValue();
					break;
				case "Foreign":
					answer = EnumChatBot.Foreign.getValue();
					break;
				default:
					answer = "Invalid";
				}

			}
		}
		// for Location
		if (StringUtils.isBlank(answer)) {
			List<String> Country = Stream.of(EnumCountry.values()).map(EnumCountry::name).collect(Collectors.toList());
			// Check base on Country
			for (String ct : Country) {
				if (StringUtils.containsIgnoreCase(question, ct)) {
					// Provide Hotel Prefer by list
					String selectedCountry = ct;
					Country country = countryrepo.findByCountryName(ct);
					List<Hotel> hotel = hotelrepo.findByCountryCode(country.getCountryCode());
					List<Hotel> hotelList = new ArrayList<Hotel>();

					for (Hotel h : hotel) {
						Hotel hotels = new Hotel();
						hotels.setHotelId(h.getHotelId());
						hotels.setHotelName(h.getHotelName());
						hotels.setAddress(EnumFunctions.Direction.getValue() + h.getAddress().replaceAll("\\s", ""));
						hotels.setCountryCode(h.getCountryCode());
						hotels.setLevel(h.getLevel());
						hotels.setLocation(h.getLocation());
						hotels.setUrl(h.getUrl());
						hotelList.add(hotels);

					}

					response.setHotelInformation(hotelList);
					answer = "Hi, Here show the Preferred Country:" + country.getCountryName() + " Hotels";
				} else {
					answer = "Invalid";
				}
			}
			if (StringUtils.equalsIgnoreCase("Invalid", answer)) {
				// Check base on Specific Location
				List<Location> location = locationrepo.findAll(Sort.by("locationName").ascending());

				for (Location l : location) {
					if (StringUtils.containsIgnoreCase(question, l.getLocationName())) {
						List<Hotel> hotel = hotelrepo.findByLocation(l.getLocationName());
						List<Hotel> hotelList = new ArrayList<Hotel>();

						for (Hotel h : hotel) {
							Hotel hotels = new Hotel();
							hotels.setHotelId(h.getHotelId());
							hotels.setHotelName(h.getHotelName());
							hotels.setAddress(EnumFunctions.Direction.getValue() + h.getAddress().replaceAll("\\s", ""));
							hotels.setCountryCode(h.getCountryCode());
							hotels.setLevel(h.getLevel());
							hotels.setLocation(h.getLocation());
							hotels.setUrl(h.getUrl());
							hotelList.add(hotels);

						}
						response.setHotelInformation(hotelList);
						answer = "Hi, Here show the Preferred Location:" + l.getLocationName() + " Hotels";
					}
				}
			}

		}
		response.setAnswer(answer);

		return response;
	}

}
