CREATE TABLE Persons (
    customer_id int NOT NULL,
    email varchar(255) NOT NULL,
    full_name varchar(255),
    age int,
    status varchar(10),
    enabled bool ,
    register_date DateTime,
    PRIMARY KEY (customer_id)
);

CREATE TABLE hotel (
    hotel_id int NOT NULL,
    hotel_name varchar(255) NOT NULL,
    level int,
    location varchar(255),
    url varchar(500),
    address varchar(500) ,
    average_Price decimal ,
    country_code varchar(10) NOT NULL,
    PRIMARY KEY (hotel_id)
);

CREATE TABLE country (
    country_id int NOT NULL,
    country_code varchar(10) NOT NULL,
    country_name varchar(255) NOT NULL,
    PRIMARY KEY (country_code)
);


